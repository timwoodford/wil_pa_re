hold off;

bit_div_loc=[-1e-10 3.9e-9 7.4e-9 1.24e-8 1.59e-8 2.09e-8 2.44e-8 2.89e-8];
bit_len_est=mean(diff(bit_div_loc));
bit_len=4.24e-9;

for ii=23:25
    [sc0_sec, sc0_V]=read_scope_csv(['scope_' int2str(ii) '.csv']);
    sc0_bit=sign(sc0_V).*double(abs(sc0_V)>1e-1);
    plot(sc0_sec, sc0_V); hold on;
    %times=sc0_sec(abs(sc0_V)<1e-8);
    %tdiffs=diff(times);
    %plot(tdiffs);
end

for ii=0:100
    x_val=-2e-10+ii*bit_len;
    line([x_val x_val], [-1 1]);
end

%xlim([-1e-7 5.5e-7]);
xlim([-1e-8 4e-8]);

sco_V=sc0_V(1000:end);
sco_sec=sc0_sec(1000:end);
z_crossings=(sco_V(2:end)>0 & sco_V(1:end-1)<0) | ...
    (sco_V(2:end)<0 & sco_V(1:end-1)>0);
plot(z_crossings);