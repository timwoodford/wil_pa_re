bits_val_1=scope_bit_converter('sector20_gain1.csv');
bits_val_2=scope_bit_converter('sector20_gain1_2.csv');

assert(all(bits_val_1==bits_val_2));

bits_val_1=scope_bit_converter('sector20_gain15.csv');
bits_val_2=scope_bit_converter('sector20_gain15_2.csv');

assert(all(bits_val_1==bits_val_2));