figure(1);
hold off;

bit_div_loc=[-1e-10 3.9e-9 7.4e-9 1.24e-8 1.59e-8 2.09e-8 2.44e-8 2.89e-8];
bit_len_est=mean(diff(bit_div_loc));
bit_len=4.24e-9;

bit_divisions=-10e-10+(0:118)*bit_len;

bit_out_vals=[];
for ii=16:17
    [sc0_sec, sc0_V]=read_scope_csv(['sector' int2str(ii) '_gain15.csv']);
    sc0_bit=sign(sc0_V).*double(abs(sc0_V)>1e-1);
    start_i=find(sc0_sec>bit_divisions(1),1);
%     sco_V=lowpass(sc0_V(start_i:end), 0.5);
    sco_V=(sc0_V(start_i:end));
    sco_sec=sc0_sec(start_i:end);
    plot(sc0_sec, sc0_V);
    hold on;
    
    sc0_bit_2=zeros(1,floor((sco_sec(end)-sco_sec(1))/bit_len));
    sc0_bit_3=zeros(1,floor(length(sc0_bit_2)/2));
    sco_bit_c=zeros(size(sco_V));
    for jj=1:length(bit_divisions)-1
        tvals=sco_sec > bit_divisions(jj) & sco_sec < bit_divisions(jj+1);
        sc0_bit_2(jj)=sign(mean(sco_V(tvals)));
        sco_bit_c(tvals)=sc0_bit_2(jj);
        
        if mod(jj,2)==0
            tvals2=sco_sec > bit_divisions(jj) & sco_sec < bit_divisions(jj+1);
            pl_val=3+sc0_bit_2(jj);
            sc0_bit_3(jj/2)=sc0_bit_2(jj)==1;
%             line([bit_divisions(jj-1) bit_divisions(jj+1)], [pl_val pl_val]);
            text(bit_divisions(jj-1), -1.1, [int2str(sc0_bit_3(jj/2))]);
            text(bit_divisions(jj-1), 1.1, [int2str(jj/2)]);
            if sc0_bit_2(jj)==sc0_bit_2(jj+1)
                fprintf('%i :(\n', jj);
            end
        end
    end
    plot(sco_sec, sco_bit_c);
    bit_out_vals=[bit_out_vals; sc0_bit_3(1:61)];
end

for ii=1:length(bit_divisions)
    line([bit_divisions(ii) bit_divisions(ii)], [-1 1]);
end

xlim([-1e-8 2e-7]);

% z_crossings=(sco_V(2:end)>0 & sco_V(1:end-1)<0) | ...
%     (sco_V(2:end)<0 & sco_V(1:end-1)>0);
% cross_times=sco_sec(z_crossings);
% cross_diffs=diff(cross_times);
% figure; plot(cross_diffs);
%
% recons_bits=sign(sco_V(1))==1;
% recons_bits=1;
% for jj=1:length(cross_diffs)
%     prev_bit=recons_bits(end);
%     this_len=double(cross_diffs(jj)>0.7e-8) + 1;
%     recons_bits=[recons_bits ~prev_bit & ones(1,this_len)];
% end


%     plot((0:length(recons_bits)-1)*bit_len-6.6e-9, recons_bits);