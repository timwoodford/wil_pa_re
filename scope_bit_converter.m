function data_bits=scope_bit_converter(filename)
bit_len=4.24e-9;
bit_divisions=-10e-10+(0:120)*bit_len;

[sc0_sec, sc0_V]=read_scope_csv(filename);
start_i=find(sc0_sec>bit_divisions(1),1);
sco_V=(sc0_V(start_i:end));
sco_sec=sc0_sec(start_i:end);

sc0_bit_2=zeros(1,floor((sco_sec(end)-sco_sec(1))/bit_len));
sc0_bit_3=zeros(1,floor(length(sc0_bit_2)/2));
sco_bit_c=zeros(size(sco_V));
for jj=1:length(bit_divisions)-1
    tvals=sco_sec > bit_divisions(jj) & sco_sec < bit_divisions(jj+1);
    sc0_bit_2(jj)=sign(mean(sco_V(tvals)));
    sco_bit_c(tvals)=sc0_bit_2(jj);
    
    if mod(jj,2)==0
        sc0_bit_3(jj/2)=sc0_bit_2(jj)==1;
    end
end

data_bits=sc0_bit_3(1:61);
end