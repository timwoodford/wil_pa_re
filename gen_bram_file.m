
% Generate CRCs
data=uint16(zeros(4096, 1));
idx=0;
for direction={'rx' 'tx'}
    for sector_num=0:127
        for gain_num=0:15
            idx_test = bitshift(int16(all(direction{1}=='tx')), 11) + bitshift(sector_num, 4) + gain_num;
            assert(idx_test == idx);
            [~, crc]=genCRC(sector_num, gain_num, direction{1});
            data(idx+1) = uint16(bi2de(crc));
            idx = idx + 1;
        end
    end
end

% Write COE file
fh=fopen('pa_ctl_crcs.coe', 'w');
fprintf(fh, '; Memory intialisation file containing BRAM LUT values\n');
fprintf(fh, '; giving CRCs for the phased array control protocol\n');
fprintf(fh, '; Memory indexing:\n');
fprintf(fh, ';  (gain_num << 8) | (direction << 7) | sector\n');
fprintf(fh, '; Direction: tx -> 0, rx -> 1\n');
fprintf(fh, 'memory_initialization_radix=16;\n');
fprintf(fh, 'memory_initialization_vector=\n');
for ii=1:length(data)
    fprintf(fh, '%s,\n', dec2hex(data(ii,:), 4));
end
fseek(fh, -2, 'eof');
fprintf(fh, ';');

% Write mem file
fh=fopen('pa_ctl_crcs.mem', 'w');
for ii=1:length(data)
    fprintf(fh, '%s\n', dec2hex(data(ii,:), 4));
end