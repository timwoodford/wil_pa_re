% Adapted from Renjie's CRC generation method
function [signal, CRC] = genCRC(sector_num, gain_num, direction)


%pre_1 = [0,0,0,0,0,0,0,1,1,1,0,0,0,1,0,1,1,1,0,1,0,0,1];
%0,1,(tx) 1,0 (rx)
pre_1 = [0,0,0,0,0,0,0,1,1,1,0,0,0,1,0,1,1,1,0,1,0,0,1];
pre_2 = [0,1,0,0,0,0];
if all(direction=='tx')
    pre = [pre_1, 0, 1, pre_2];
elseif all(direction=='rx')
    pre = [pre_1, 1, 0, pre_2];
else
    error('Unrecognized direction');
end

sector = de2bi(sector_num,7);
gain = de2bi(gain_num,4);

input_bits = [pre, sector, gain];

CRC = [0,0,0,1,0,1,1,0,0,0,1,0,0,1,0,0];

for i = 1:length(input_bits)
    in = xor(input_bits(i),CRC(16));
    CRC = [0 CRC(1:15)];
    CRC(1)= xor(in,CRC(1));
    CRC(6)= xor(in,CRC(6));
    CRC(13)= xor(in,CRC(13));
end

CRC=fliplr(CRC);
signal=[input_bits CRC];

end