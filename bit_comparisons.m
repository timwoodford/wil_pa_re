gain_comparison_bits=[];
sector=20;
for gain=[1 5 15]
    gain_comparison_bits=[gain_comparison_bits; ...
        scope_bit_converter(['sector' int2str(sector) '_gain' ...
        int2str(gain) '.csv'])];
end
csvwrite('bits_gain.csv', [[1;5;15] gain_comparison_bits]);

sector_comparison_bits=[];
gain=15;
for sector=1:20
    sector_comparison_bits=[sector_comparison_bits; ...
        scope_bit_converter(['sector' int2str(sector) '_gain' ...
        int2str(gain) '.csv'])];
end

csvwrite('bits_sectors.csv', [(1:20).' sector_comparison_bits]);