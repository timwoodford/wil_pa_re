sample_rate=1e9;
bit_len=4.24e-9;
pad_len=floor(1e-6*sample_rate);
sector=1;

known_bit_vals=csvread('bits_sectors.csv');
bits_row=find(known_bit_vals(1,:)==sector,1);

% Guess is we're missing about 3 values at beginning and 3 at end
% This sums to a signal length of 68 bits, which seems like it might be
% wrong
bit_values=[0 0 0 known_bit_vals(bits_row,:) 0 0 0];

unscaled_vals=[zeros(1,pad_len) 2*bit_vals-1 zeros(1,pad_len)];
scaled_vals=2^(13)*unscaled_vals; % Need to double-check this