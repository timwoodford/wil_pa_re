close all;

[sc0_sec, sc0_V]=read_scope_csv('scope_0.csv');

fs=1/mean(diff(sc0_sec(3:end)))

sz=128;

figure; plot(sc0_sec, sc0_V);

figure; spectrogram(sc0_V, sz, sz-1, sz, fs, 'yaxis');

% [sc1_sec, sc1_V]=read_scope_csv('scope_1.csv');
% 
% figure; plot(sc1_sec, sc1_V);
% 
% figure; spectrogram(sc1_V, 64, 63);