* Bits 33-37 are the sector index, going from LSB to MSB.  Bit 38 might also be involved here, but we haven't gone to a high enough sector index to find out.
* Bits 1-32 appear to be constant (1 and 2 are NaN on sector 12, need to investigate further)
* Bits 39-43 are constant
* Bits 44-46 is another number.  From sectors 1-7, it increases sequentially (goes MSB to LSB).  After that, it is somewhat inconsistent.
* Everything else up to bit 60 changes with an unknown pattern
* Bit 61 is a constant 0
